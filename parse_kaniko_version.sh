#!/bin/sh

sed '/^FROM/!d' Dockerfile | tail -n 1 | cut -d'v' -f2 | cut -d'-' -f1
